# Tsur

This plugin adds a Content Management System (CMS) to your Routify/Svelte site, that you can manage from `yourwebsite.com/admin`.

## Usage
In your config insert:

```js
import tsurCMS from 'tsur';
// ...
  routify({
      plugins: [tsurCMS()]
    })
```